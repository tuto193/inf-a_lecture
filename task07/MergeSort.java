import AlgoTools.IO;

public class MergeSort {

    /**
     * Diese Operation mischt die Arrays links und rechts.
    */
    public static int[] merge (int[]links, int[]rechts) {

        // Laufindizes
        int i = 0, j = 0, k = 0;

        // Platz fuer Folge ergebnis besorgen
        int[] ergebnis = new int[links.length + rechts.length];

        // mischen, bis ein Array leer
        while ((i < links.length) && (j < rechts.length)) {

        // jeweils das kleinere Element wird nach ergebnis uebernommen
        if (links[i] < rechts[j])
            ergebnis[k++] = links[i++];
        else
            ergebnis[k++] = rechts[j++];
        }

        // ggf.: Rest von Folge links
        while (i < links.length) {
            ergebnis[k++] = links[i++];
        }
        // ggf.: Rest von Folge rechts
        while (j < rechts.length) {
            ergebnis[k++] = rechts[j++];
        }

        // Ergebnis abliefern
        return ergebnis;
    }

    public static int[] copyArray( int[] array, int beg, int end ) {
        int[] half = new int[end - beg + 1];
        int i;
        for( i = 0; i < half.length; i++ ) {
            half[i] = array[beg++];
        }
        return half;
    }

    public static int[] sort( int[] a ) {
        int i, l = a.length;
        for( i = 0; i < l - 1; i++ ) {
            if( a[i] > a[i + 1] ) {
                int tmp = a[i];
                a[i] = a[i + 1];
                a[i + 1] = tmp;
            }
        }
        return a;
    }

    public static int[] sortRekursiv( int[] array ) {
        int[] a, b;
        int half = array.length / 2;
        if( array.length <= 2 ) {
            array = sort( array );
            return array;
        } else {
            a = copyArray(array, 0, half );
            b = copyArray(array, half + 1, array.length - 1 );
            a = sortRekursiv(a);
            b = sortRekursiv(b);
            array = merge(a, b);
            return array;
        }
    }
// 6 4 65 7 3 4 53 234 4567 3 33 7 5 7 10
    public static void main( String[] args ) {
        int[] array;
        do{
           array = IO.readInts("Gebe ein Int array ein: " );
        } while( array.length < 2 );
        array = sortRekursiv(array);

        for( int i = 0; i < array.length; i++ ) {
            IO.print(array[i] + " ");
        }
        IO.println();
    }
}