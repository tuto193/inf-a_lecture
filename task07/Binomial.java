import AlgoTools.IO;

public class Binomial {
    public static long binKoeff( long n, long k ) {
        // long nMINk = n - k;
        // // nMINk darf nicht 0 sein
        // nMINk = nMINk < 1? 1 : nMINk;
        if( n >= k ) {
            // Erster Anker
            if( n == k ) {
                return 1;
            } else if( k > 0 ) {
                // Rekursion nach Wikipedia's Multiplicative Formula
                return ( (n + 1 - k) * binKoeff( n, k - 1 ) ) / k;
            } else if ( k == 0 ) {
                // Zweiter Anker
                return 1;
            }
        }
        IO.println( "Error: k muss kleiner/gleich n sein" );
        return 0;
    }

    public static void main( String[] args ) {
        long n, k;
        do {
        n = IO.readLong("Gebe ein 'n' ein: " );
        k = IO.readLong("Gebe ein 'k' ein: " );
        } while( n < k );

        IO.println( n + " ueber " + k + " = " + binKoeff(n, k) );

    }
}