import AlgoTools.IO;

public class BinSearch {
    public static void searchBinary( int[] array, int zahl, int anfang, int ende ) {
        if( anfang <= ende ) {
            int mid = (anfang + ende) / 2;
            if( array[mid] == zahl ) {
                IO.println( "Zahl " + zahl + " bei Index " + mid + " gefunden. " );
            } else if( array[mid] < zahl ) {
                searchBinary(array, zahl, mid + 1, ende);
            } else {
                searchBinary(array, zahl, anfang, mid);
            }
        } else {
            // Wenn anfang und ende nicht mehr stimmen
            IO.println( "Zahl " + zahl + " ist nicht in array." );
        }
    }

    public static void main( String[] args ) {
        int[] array;
        int zahl;

        do{
            array = IO.readInts( "Gebe ein (sortiertes) Array ein: " );
        } while( array.length < 2 );

        zahl = IO.readInt( "Gebe eine zu suchende Zahl ein: " );
        searchBinary(array, zahl, 0, array.length - 1);
    }
}