import AlgoTools.IO;

public class DoStuff {

    public static void doSomething(int a[]) {
        int n = a.length;
        int g = n;
        boolean b = true;

        while (g != 1 || b == true) {
            g = doSomethingElse(g);
            b = false;
            for (int i = 0; i < n - g; i++) {
                if (a[i] > a[i + g]) {
                    int t = a[i];
                    a[i] = a[i + g];
                    a[i + g] = t;
                    b = true;
                }
                IO.print( "For Loop bei g = " + g );
                IO.print( " und i = " + i );
                IO.println();
                for( int j = 0; j < n; j++ ) {
                    IO.print(a[j] + " ");
                }
                IO.println();
                IO.println();
            }
        }
    }

    public static int doSomethingElse(int h) {
        h = (h * 10) / 13;
        if (h < 1) {
            return 1;
        }
        return h;
    }

    public static void main(String[] args) {
        int[] a;
        do {
            a = IO.readInts();
        } while (a.length == 0);
        doSomething(a);
        for (int i = 0; i < a.length; i++) {
            IO.print(a[i] + " ");
        }
    }

}