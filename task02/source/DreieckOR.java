package source;

import AlgoTools.IO;

// Aufgabe 2.3c

public class DreieckOR{

  public static void main (String [] args) {


    int h;

    do {
      h= IO.readInt("Hoehe der Dreiecksmatrix eingeben: ");
    } while( h < 2);


    // Matrix zeichnen

    for (int i = h - 1; i >= 0; i--) {
    	IO.print(" ", h - i);
      for (int j = 0; j <= i; j++) {
        IO.print("*");
      }
      IO.println();
    }

  }
}
