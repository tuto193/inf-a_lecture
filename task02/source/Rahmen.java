package source;

import AlgoTools.IO;

// Aufgabe 2.3b

public class Rahmen {

  public static void main (String [] args) {

    int m, n;
    int zeile, spalte;

    do {
      m = IO.readInt("Zeilenzahl eingeben: ");
    } while (m < 3 );

    do {
      n = IO.readInt("Spaltenzahl eingeben: ");
    } while ( n < 3 );


    // Rahmen zeichnen

    for( zeile = 0; zeile < m; zeile++ ) {
      for( spalte = 0; spalte < n; spalte++ ) {
        if( zeile == 0 || zeile == m - 1) {

          IO.print("*");

        } else{

          if (spalte == 0 || spalte == n-1) {
            IO.print("*");
          } else {
            IO.print(" ");
          }

        }

      }
      IO.println();
    }

  }

}
