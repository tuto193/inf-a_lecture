package source;

import AlgoTools.IO;

public class TeilerTabelle {
	public static void main( String[] args ) {
        int k;
        int i, j; // our indices
        do {
            k = IO.readInt("Bis welche Zahl wollen wir checken?[1-20]: ");
        } while( k < 0 || k > 21 );
        
        for( i = 0; i <= k; i++ ) {
            if( i > 0 ) {
                // Draw the left boxes
                if( i < 10 ) {
                    IO.print(" ");
                    IO.print(i);
                    IO.print(" |");
                } else {
                    IO.print(i);
                    IO.print(" |");
                }
            }
            for( j = 0; j <= k; j++ ) {
                if( i == 0 ) {
                    // Draw the first row and the bar below it
                    if( j == 0 ) {
                        IO.print("   |"); 
                    } else {
                        IO.print( "  ");
                        IO.print( j );
                        if( j < 10 ) {
                            IO.print( " " );
                        }
                    }
                    if( j == k ) {
                        IO.println();
                        IO.print("---+");
                        for( int x = 0; x < k * 4; x++ ) {
                            IO.print("-");
                        }
                    }
                }
                if( j > 0 && i > 0 ) {
                    IO.print("  ");
                    if( j % i == 0 ) {
                        IO.print("+ ");
                    } else {
                        IO.print("  ");
                    }
                }
            }
            IO.println();
        } 

	}
}
