package source;

import AlgoTools.IO;

public class Raute {
    public static void main( String args[] ) {
        int s;

        do {
            s = IO.readInt("Lange der Seite der Raute eingeben: ");
        } while( s < 2);
        // Raute Zeichnen
        int space, star;
        // The top of the thingy
        for (int i = 0; i < s; i++) {
            space = s - i - 1;
            star = 1 + (i*2);
            if( space > 0 ) {
                IO.print(" ", space ); 
            }
            for( int x = 0; x < star; x++ ) {
                IO.print("*");
            }
            IO.println();
        } 
        // The bottom of the thingy
        for( int j = s - 2; j >= 0; j-- ) {
            space = s - j - 1;
            star = 1 + (j*2);
            if( space > 0 ) {
                IO.print( " ", space);
            }
            for( int y = star; y > 0; y-- ) {
                IO.print( "*");
            }
            IO.println();
        }
    }
}
