import AlgoTools.IO;

public class Unique {
    public static void main( String[] args ) {
        int i, ii;
        boolean doppelte = false;
        int[] test;
        do{
            test = IO.readInts( "Bitte ein Int-Array eingeben: " );
        } while( test.length < 1 );

        // Check fuer doppelte
        for( i = 0; i < test.length && !doppelte; i++ ) {
            for( ii = i + 1; ii < test.length && !doppelte; ii++ ) {
                // Wenn doppelte gefunden, beende Schleifen
                if( test[i] == test[ii] ) {
                    doppelte = true;
                    break;
                }
            }
        }

        if( doppelte ) {
            IO.println( "Es kommen einige Elemente doppelt vor! " );
        } else {
            IO.println( "Alle Elemente kommen nur einmal vor!" );
        }
    }
}