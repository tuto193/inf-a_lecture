import AlgoTools.IO;

public class GnomeSort {

  /**
  * Diese Operation gibt ein Integer-Array auf der Konsole aus.
  *
  * @param array welches ausgegeben werden soll.
  */
  public static void printArray(int[] array) {
    for (int i = 0; i < array.length; i++) {
      IO.print(" " + array[i]);
    }
    IO.println();
  }


  /**
  * Diese Operation liest ein Integer-Array und gibt dieses nur dann zurück,
  * wenn dieses mindestens die Länge 1 hat.
  *
  * @return eingelesenes Array.
  */
  public static int[] readArray() {
    // Variablendeklaration
    int[] array;

    // Sicheres Einlesen des Arrays
    do {
      array = IO.readInts("Unsortiertes Array: ");
    } while (array.length == 0);
    return array;
  }

  public static void sort( int[] a ) {
      int i, smallest, actual, tmp;

      for( i = 0; i < a.length - 1; i++ ) {
            do{
                smallest = i;
                actual = i + 1;
                // if the actual is smaller than the left one, we swap until we it's either bigger or at the beginning
                // swap arround, and check
                if( a[smallest] > a[actual] ) {
                    tmp = a[smallest];
                    a[smallest] = a[actual];
                    a[actual] = tmp;
                    i--;
                } else {
                    break;
                }
            } while( i >= 0 );
        }
  }


  /**
  * Die Hauptoperation der Lösung von GnomeSort.
  * Hier erfolgt die Variablendeklaration des (un-)sortierten Arrays und
  * ansonsten nur die Operationsaufrufe.
  *
  * @param args Kommandozeilenparameter
  */
  public static void main(String[] args) {
    IO.println();

    // Variablendeklaration
    int[] unsortedArray = readArray();
    IO.println();

    // Ausgabe des ursprünglichen Arrays
    IO.print("Unsortiertes Array  : ");
    printArray(unsortedArray);

    // Hier wird die Operation sort() aufgerufen. Diese implementiert den
    // Gnome-Sort-Algorithmus.
    sort(unsortedArray);


    // Ausgabe des sortierten Arrays
    IO.print("Sortiertes Array    : ");
    printArray(unsortedArray);

    IO.println();
  }

}
