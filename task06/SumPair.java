import AlgoTools.IO;

public class SumPair {
    public static void main( String[] args ) {
        int i, ii;
        int zahl;
        boolean gefunden = false;
        int[] test;
        do{
            test = IO.readInts( "Bitte ein Int-Array eingeben: " );
        } while( test.length < 1 );

        zahl = IO.readInt( "Bitte eine Zahl eingeben: " );

        // Check fuer gefunden
        for( i = 0; i < test.length && !gefunden; i++ ) {
            for( ii = i + 1; ii < test.length && !gefunden; ii++ ) {
                // Wenn gefunden gefunden, beende Schleifen
                if( test[i] + test[ii] == zahl ) {
                    gefunden = true;
                    break;
                }
            }
        }

        if( gefunden ) {
            IO.println( "Es gibt mindestens ein Paar mit der Summe " + zahl );
        } else {
            IO.println( "Es gibt kein Paar mit der Summe " + zahl );
        }
    }
}