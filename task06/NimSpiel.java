import AlgoTools.IO;

public class NimSpiel {

    public static char[][] board;
    public static int rows;
    public static int columns;

    /**
     * Initializire das Spielfeld mit Leerzeichen und die Staebchen
     * @param b das Spielfeld
     * @param r die Zeilen
     * @param c die Spalten
     */
    public static void initializeBoard( char[][] b, int r, int c ) {
        int padding = 0;
        for( int i = r - 1; i >= 0; i-- ) {
            for( int ii = 0; ii < c; ii++ ) {
                if( ii >= padding && ii < c - padding ) {
                    b[i][ii] = '|';
                } else {
                    b[i][ii] = ' ';
                }
            }
            padding++;
        }
    }

    /**
     * Gibt zurueck, ob ein Zug moeglich ist (Die eingegebenen Koordinaten
     * existieren im Brett und sie sind kein Leerzeichen)
     */
    public static boolean checkPossible( char[][] b, int r, int c ) {
        return b[r][c] == '|';
    }

    /**
     * Check if the row is NOT empty. Returns true when not empty, false otherwise
     * @param b     the game board
     * @param r     the row to check
     * @return      check if the player can continue choosing
     */
    public static boolean checkRow( char[][] b, int r ) {
        for( int i = 0; i < columns; i++ ) {
            if( b[r][i] == '|' ) {
                return true;
            }
        }
        IO.println( "Die Zeile ist leer..." );
        return false;
    }

    /**
     * Fuehre einen Zug aus, mit einem Spieler
     */
    public static void makeTurn( char[][]b, char player ) {
        int row, column, lastColumn = -1;
        do{
            row = IO.readInt( "Spieler " + player + ", gebe eine Zeile ein: " );
        } while( ( row < 0 || row > rows - 1) || !checkRow(b, row) );

        while( checkRow(b, row ) ) {
            do{
                column = IO.readInt( "Spieler " + player + ", gebe eine Spalte ein[dieselve Spalte als vorher zum Beenden des Zuges]: " );
            } while( (column < 0 || column > columns - 1) || !checkPossible(b, row, column) );
            if( lastColumn != column ) {
                b[row][column] = ' ';
                lastColumn = column;
                printBoard( b );
            } else {
                break;
            }
        }


    }

    /**
     * Zeige das Spielbret
     * @param b spielbrett
     */
    public static void printBoard( char[][]b ) {
        for( int i = 0; i < rows + 1; i++ ) {
            for( int j = 0; j < columns + 1; j++ ) {
                if( i == 0 ) {
                    if( j > 0 ) {
                        if( j - 1 < 10 ) {
                            IO.print("  ");
                        } else {
                            IO.print( " " );
                        }
                        IO.print( j - 1);
                    } else {
                        IO.print( " " );
                    }
                } else if( j == 0 ) {
                    IO.print( i - 1 );
                } else {
                    IO.print("  ");
                    IO.print( b[i - 1][j - 1] );
                }
            }
            IO.println();
        }
    }

    /**
     * Testet ob das Spiel vorbei ist
     * @param b         das brett
     * @param spieler   der Spieler
     * @return          Zustand des Spieler (true == vorbei)
     */
    public static boolean checkOver( char[][] b, char spieler ) {
        for( int i = 0; i < rows; i++ ) {
            for( int j = 0; j < columns; j++ ) {
                if( b[i][j] == '|' ) {
                    return false;
                }
            }
        }
        IO.println( "Spieler " + spieler + " hat gewonnen! " );
        return true;
    }

    public static void main( String[] args ) {
        boolean spielerA = true;
        char spieler = 'A';
        do{
            rows = IO.readInt( "Bitte Anzahl an Zeilen eingeben [ > 2]: " );
        } while( rows < 3 );
        columns = ((rows - 1) * 2) + 1;

        // Initializire Spielfeld
        board = new char[rows][columns];

        initializeBoard( board, rows, columns );
        while( !checkOver(board, spieler) ) {
            printBoard(board);
            if( spielerA ) {
                spieler = 'A';
                makeTurn( board, spieler );
                spielerA = false;
            } else {
                spieler = 'B';
                makeTurn(board, spieler );
                spielerA = true;
            }
        }
    }
}