import AlgoTools.IO;

public class a1 {
    public static void main( String[] args ) {
        String s1, s2;
        int i, j;
        boolean esIstSo = false;
        do {
            s1 = IO.readString("Bitte ein String: ");
        } while (s1.isEmpty());
        do {
            s2 = IO.readString("Bitte noch ein String: ");
        } while (s2.isEmpty() || s2.length() > s1.length());
        for (i = 0; i < (s1.length() - s2.length()) + 1 && !esIstSo; i++) {
            for (j = 0; j < s2.length(); j++ ) {
                if (s1.charAt(i + j) != s2.charAt(j)) {
                    esIstSo = false;
                    break;
                } else {
                    esIstSo = true;
                }
            }
        }
        IO.println("Ist es so? " + esIstSo);

    }
}