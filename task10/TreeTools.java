import AlgoTools.IO;

/**
* Dies Klasse stellt Operationen zur Verfuegung um einen Binärbaum der Klasse
* LinkedTree auf der Konsole auszugeben und dessen Tiefe zu ermitteln.
*/
public class TreeTools {

    /**
     * Check the depth of the tree based on the longest path
     * from the very top to the bottom of the deepest
     * child-tree.
     * @param tree  the tree (and which's child's) we are checking
     * @return  the depth of the actual tree
     */
    public static int determineDepth( LinkedTree tree ) {
        int depthL = 1;
        int depthR = 1;
        LinkedTree left = tree.getLeftTree();
        LinkedTree right = tree.getRightTree();
        // Check which side of the (child)tree is deeper
        if( tree.isEmpty() ) { depthL--; depthR--; }

        if( !right.isEmpty() ) {
            depthR += determineDepth(right);
        }
        if( !left.isEmpty() ) {
            depthL += determineDepth(left);
        }
        // Return the biggest of our two found depths
        return depthL > depthR? depthL:depthR;
    }

    /**
     * Print the actual level of the tree we are on
     * @param tree  the tree we are printing
     * @param level the level on the tree we re printing
     * @param spaces    the amount of spaces we want between
     *                  the nodes we want to print
     */
    public static void printLevel( LinkedTree tree, int level, int spaces ) {
        if( level == 0 ) {
            // Because the IO.print function is extremely dumb and can't
            // tell a 0 from a 1 apart
            if( spaces > 0 ) {
                IO.print(" ", spaces );
            }
            IO.print( tree.getRootData() );
            IO.print(" ", spaces );
        } else {
            if( !tree.getLeftTree().isEmpty() ) {
                printLevel(tree.getLeftTree(), level - 1, spaces );
            } else {
                // If we didn't print a left side, we still need
                // the spaces that it would have had in between
                if( spaces > 0 ) {
                    IO.print(" ", (spaces * 2) );
                }
            }
            if( spaces > 0 ) {
                IO.print(" ", spaces/2);
            }
            if( !tree.getRightTree().isEmpty() ) {
                printLevel(tree.getRightTree(), level - 1, spaces);
            }
        }
    }

    /**
     * Druckt einen LinkedTree auf der Konsole ebenenweise aus.
     * Nutzt detemineDepth(LinkedTree), printLevel(LinkedTree,int,int) und
     * spaces(int).
     */
    public static void printTree(LinkedTree tree) {

        // Berechne die Tiefe des Baumes.
        int remainingDepth = determineDepth(tree);

        // Gehe die Ebenen des Baumes durch.
        for(int i = 0; i < remainingDepth; i++) {
            // Drucke die Ebene, beruecksichtige Anzahl der Leerzeichen
            // fuer korrekte Einrueckung.
            printLevel(tree, i, spaces(remainingDepth - i));
            IO.println();
            IO.println();
        }
    }

    /**
     * Berechnet die Anzahl an benoetigten Leerzeichen fuer
     * eine korrekte Einrueckung, also 0.5 * (2^(level) - 2).
     */
    private static int spaces(int level) {

        if(level == 1) {
            return 0;
        }
        else {
            //verdoppele die Leerzeichen gegenueber der Ebene darunter
            //und fuege ein weiteres Leerzeichen hinzu
            return 2 * spaces(level - 1) + 1;
        }
    }

}
