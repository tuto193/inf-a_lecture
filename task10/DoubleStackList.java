/**
 * Small class to represent a DoubleStackList
 * It comprises of one object between to lists, which remain in the same position
 * relative to the object current. That means that current will always be after
 * and before back, regardless of the elements inside these other LinkedStacks
 */
public class DoubleStackList {
    private LinkedStack front, back;
    private Object current;

    /**
     * Simple constructor
     */
    public DoubleStackList() {
        this.front = new LinkedStack();
        this.back = new LinkedStack();
        this.current = null;
    }

    /**
     * Check if our list is empty
     * @return true if no current and the member lists are empty
     */
    public boolean isEmpty() {
        return front.isEmpty() && back.isEmpty() && current == null;
    }

    /**
     * Get the current element
     * @return  the current Object that is being pointed at
     */
    public Object getCurrent() {
        return current;
    }

    /**
     * empties all objects from front into back except from the last one,
     * which becomes current
     */
    void toFirst() {
        while( hasPrev() ) {
            toPrev();
        }
    }

    /**
     * Same as toFirst() but from back into front
     */
    void toLast() {
        while( hasNext() ) {
            toNext();
        }
    }

    /**
     * Check that back has an element on top
     * @return true if back still has an element on its top (if it's not empty)
     */
    boolean hasNext() {
        return this.back.top() != null;
    }

    /**
     * same as hasNext but with front
     * @return
     */
    boolean hasPrev() {
        return this.front.top() != null;
    }

    /**
     * Push current into front and pop the top one of back out into current.
     * Only if front is not empty
     */
    void toNext() {
        if( hasNext() ) {
            if( current != null ) {
                front.push( current );
            }
            current = back.pop();
        }
    }

    /**
     * Push current into the back. Pop the top one of front into current.
     * Only if front is not empty
     */
    void toPrev() {
        if( hasPrev() ) {
            if( current != null ) {
                back.push( current );
            }
            current = front.pop();
        }
    }

    /**
     * Push current into the back and the input element becomes our current one
     * @param toAdd the element we want to add to our list
     */
    void insertAfter( Object toAdd ) {
        if( toAdd == null ) {
            // not adding a null object
            return;
        }
        this.back.push( toAdd );
        toNext();
    }

    /**
     * Push current into the front and the input element becomes the current one
     * @param toAdd the element we want to add to our list
     */
    void insertBefore( Object toAdd ) {
        if( toAdd == null ) {
            // Not adding a null object
            return;
        }
        this.front.push( toAdd );
        toPrev();
    }

    /**
     * Delete the current element and push the rest from the back to the front
     * if possible, otherwise the ones from the front to the back.
     * If front and back are empty, just set current to null
     */
    void delete() {
        if( current == null ) {
            // No need to delete if there is nothing to delete
            return;
        }
        if( !back.isEmpty() ) {
            current = back.pop();
        } else if( !front.isEmpty() ) {
            current = front.pop();
        } else {
            current = null;
        }

    }
}