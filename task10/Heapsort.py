
def heapsort( array:list ):
    count:int = len(array)
    end = count - 1
    heapify(array, count)
    while( end > 0 ):
        temp = array[end]
        array[end] = array[0]
        array[0] = temp
        end = end - 1
        siftDown( array, 0, end )

def heapify( array:list, count:int ):
    start:int = int((count - 2)/2)

    while( start >= 0 ):
        siftDown( array, start, count - 1 )
        start = start - 1

def siftDown( array:list, start:int, end:int ):
    root:int = start

    while( (root*2)+1 <= end ):
        child:int = (root*2) + 1
        swap:int = root

        if( array[swap] < array[child] ):
            temp = array[swap]
            array[swap] = array[child]
            array[child] = temp

        if( child+1 <= end and array[swap] < array[child+1] ):
            temp = array[swap]
            array[swap] = array[child+1]
            array[child+1] = temp

        if swap == root:
            return
        else:
            temp = array[root]
            array[root] = array[swap]
            array[swap] = temp
            root = swap

tree:list = [7, 3, 42, 35, 108, 1, 12, 5, 89]

heapsort(tree )
print(tree)