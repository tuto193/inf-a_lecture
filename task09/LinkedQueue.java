/**
 * A small class to represent a linkedqueue
 */
public class LinkedQueue {
    // The element at the top/front of the queue
    private Element top;
    // The element at the back/bottom of the queue
    private Element bottom;

    /**
     * Simple constructor that sets our top and bottom to null
     */
    public LinkedQueue() {
        this.top = null;
        this.bottom = null;
    }

    /**
     * Check if our list is empty. Returns true if is empty
     */
    public boolean isEmpty() {
        return bottom == null && top == null;
    }

    /**
     * View the data held by the item at the top of the queue
     */
    public Object peek() {
        return this.top.getData();
    }

    /**
     * Add an Element with data d to the back of the queue
     * @param d the data to be added with the element
     */
    public void enqueue( Object d ) {
        // if our queue was empty
        if( this.bottom == null ) {
            Element newOne = new Element( d );
            this.top = newOne;
            this.bottom = newOne;
        } else if( this.top == this.bottom ) {
        // If there was just one single element in it
            this.bottom = new Element( d, this.top );

        } else {
        // Our list had more than just a single element in itself
            Element toAdd = new Element( d, this.bottom );
            this.bottom = toAdd;
        }
    }

    /**
     * Pop the element at the top of the queue, and return the data
     * that it was holding
     *
     * @return  the Object that was being held by the item deleted
     */
    public Object dequeue() {
        // If the list is empty
        Element toDelete;
        if( this.isEmpty() ) {
            return null;
        } else if( this.top == this.bottom ) {
        // if there is just one single element in it
            toDelete = this.top;
            this.top = null;
            this.bottom = null;
            return toDelete.getData();
        } else {
        // There is more than just one element in the list
            Element newTop = this.bottom;
            while( newTop.getNext() != this.top ) {
                // Find the element under the top
                newTop = newTop.getNext();
            }
            // Prepare to delete the top
            toDelete = this.top;
            newTop.setNext( null );
            this.top = newTop;
            return toDelete.getData();
        }
    }
}