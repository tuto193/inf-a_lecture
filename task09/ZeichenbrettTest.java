import AlgoTools.IO;

public class ZeichenbrettTest {
    public static void main( String[] args ) {
        Zeichenbrett board;
        int w = 0, h = 0, x = -1, y = -1;
        char color;
        String option;
        boolean fill;

        do{
            w = IO.readInt( "Gebe eine Breite ein: " );
        } while( w < 1 );

        do{
            h = IO.readInt( "Gebe eine Hoehe ein: " );
        } while( w < 1 );

        board = new Zeichenbrett( w, h );

        board.gebeAus();
        while( true ) {

            do{
                option = IO.readString( "Gebe eine Option ein ['setzen' oder 'floodfill']: " );
            } while( !option.equals( "setzen" ) && !option.equals( "floodfill" ) );

            if( option.equals( "setzen" ) ) {
                fill = false;
            } else {
                fill = true;
            }

            do {
                x = IO.readInt( "Gebe eine X-Koordinate ein: " );

                y = IO.readInt( "Gebe eine Y-Koordinate ein: " );

            } while( !board.validIndex( x, y ) );


            // Get a new color to draw with
            color = IO.readChar( "Gebe eine Farbe ein: " );
            if( !fill ) {
                board.setFarbe( x, y, color );
            } else {
                FloodFill.floodFill( board, x, y, color );
            }

            board.gebeAus();
            IO.println();
        }
    }
}