import AlgoTools.IO;

/**
* Diese Klasse stellt ein Zeichenbrett, bestehend aus ASCII-Zeichen, zur
* Verfuegung.
*/
public class Zeichenbrett {

    private char[][] zeichenbrett;
    private int breite, hoehe;

    public Zeichenbrett( int b, int h ) {
        this.breite = b;
        this.hoehe = h;
        // Das Zeichenbrett hat die Groesse 10x10.
        this.zeichenbrett = new char[hoehe][breite];

        // Das Zeichenbrett besteht aus Leerzeichen und hat einen Rahmen aus '#'.
        this.initialisiereZeichenbrett();

        this.zeichneInitialesMuster();
    }

    /**
        * Oeffentlicher Konstruktor der Klasse Zeichenbrett.
        * Erstellt ein char-Array, welches das Zeichenbrett repraesentiert und fuellt
        * dieses mit Leerzeichen.
    */
    public Zeichenbrett() {
        this( 10, 10 );
    }

    /**
     * Diese Operation liefert die Zeilenanzahl des Zeichenbretts zurueck.
    */
    public int getZeilen(){
        return this.zeichenbrett.length;
    }

    /**
     * Diese Operation liefert die Spaltenanzahl des Zeichenbretts zurueck.
    */
    public int getSpalten(){
        return this.zeichenbrett[0].length;
    }

    /**
     * Diese Operation gibt das Zeichenbrett auf der Konsole aus.
    */
    public void gebeAus() {
        // Stelle das Zeichenbrett Zeichen fuer Zeichen dar.
        for( int zeile = 0; zeile < zeichenbrett.length; zeile++ ) {
            for( int spalte = 0; spalte < zeichenbrett[zeile].length; spalte++ ) {
                IO.print(zeichenbrett[zeile][spalte]);
            }
        IO.println();
        }
    }

    /**
     * Diese Operation bereitet das Zeichenbrett vor. Es wird mit Leerzeichen
     * gefuellt.
    */
    private void initialisiereZeichenbrett() {
        int zeile, spalte;

        // Fuelle das Zeichenbrett mit Leerzeichen.
        for ( zeile = 0; zeile < zeichenbrett.length; zeile++ ) {
            for ( spalte = 0; spalte < zeichenbrett[zeile].length; spalte++ ) {
                zeichenbrett[zeile][spalte] = ' ';
            }
        }

        // Zeichne den Rahmen und die erste und letzte Spalte.
        for ( zeile = 0; zeile < zeichenbrett.length; zeile++ ) {
            zeichenbrett[zeile][0] = '#';
            zeichenbrett[zeile][zeichenbrett[zeile].length - 1] = '#';
        }

        // Zeichne die erste und letzte Zeile.
        for ( spalte = 0; spalte < zeichenbrett[0].length; spalte++ ) {
            zeichenbrett[0][spalte] = '#';
            zeichenbrett[zeichenbrett.length - 1][spalte] = '#';
        }
    }

    /**
     * Check whether the given index can be reached within the array. Returns true if yes
     */
    public boolean validIndex( int x, int y ) {
        if( x < 0 || x >= this.breite || y < 0 || y >= this.hoehe ) {
            return false;
        }
        return true;
    }

    /**
     * Draw the diagonal line accross the middle of the board
     */
    private void zeichneInitialesMuster() {
        this.initialisiereZeichenbrett();
        int x, y;
        for( y = 0; y < this.hoehe; y++ ) {
            for( x = 0; x < this.breite; x++ ) {
                if( x == y ) {
                    setFarbe( x, y, '#' );
                } else {
                    setFarbe(x, y, ' ');
                }
            }
        }
    }

    /**
     * Set a field of the 2D array to the given "color" if the index is valid.
     * @param x the x coordinate to be drawn on
     * @param y the y coordinate to be drawn on
     * @param neueFarbe the char representing the color we are drawing with
     */
    public void setFarbe( int x, int y, char neueFarbe ) {
        if( this.validIndex( x, y ) ) {
            this.zeichenbrett[y][x] = neueFarbe;
        }
    }

    /**
     * Returns the "color" of the entered coordinate inside the array. Returns '0' if
     * the coordinates where not valid
     * @param x the x coordinate to get
     * @param y the y coordinate to get
     * @return  the "color" of the field at the given coordinates
     */
    public char getFarbe( int x, int y ) {
        if( this.validIndex( x, y ) ) {
            return this.zeichenbrett[y][x];
        }
        // Invalid Index
        return '0';
    }
}
