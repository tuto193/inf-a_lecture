import AlgoTools.IO;

/**
* Diese Klasse dient zum Testen der Klasse LinkedQueue.
*/
public class LinkedQueueTest {

  public static void main(String[] args) {
    IO.println();

    LinkedQueue queue = new LinkedQueue();

    IO.println("Die neu konstruierte Queue ist leer: " + queue.isEmpty());

    queue.enqueue(new Object());
    IO.print("Nach einer Add-Operation ist die Queue nicht leer: ");
    IO.println(!queue.isEmpty());

    queue.dequeue();
    IO.print("Nach einer Add-Remove-Operation ist die Queue unverändert, ");
    IO.print("also wieder leer: ");
    IO.println(queue.isEmpty());

    Object x = new Object();
    queue.enqueue(x);
    IO.print("Nach der Add-Operation mit dem Objekt x, ");
    IO.print("liefert die Element-Operation das Objekt x: ");
    IO.println(x.equals(queue.peek()));

    IO.print("Die Remove-Operation liefert das Objekt x ...: ");
    IO.println(x.equals(queue.dequeue()));

    IO.print("und entfernt dieses zugleich, sodass der Stack anschließend ");
    IO.print("wieder leer ist: ");
    IO.println(queue.isEmpty());

    IO.println();

  }
}
