import AlgoTools.IO;

public class FloodFill {

    public static void floodFill( Zeichenbrett board, int x, int y, char color ) {
        ArrayStack toDraw = new ArrayStack();
        // We just add things to our stack, if we get
        int[] first = { x, y };
        toDraw.push( (Object) first );

        while( !toDraw.isEmpty() ){

            int[] pixel = (int[]) toDraw.pop();
            int drawX = pixel[0];
            int drawY = pixel[1];
            // We are on a valid index
            if( board.validIndex( drawX, drawY ) ) {
                // We can actually draw on this spot
                if( board.getFarbe( drawX, drawY ) == ' ' ) {
                    // Color the pixel we are on
                    board.setFarbe( drawX, drawY, color );

                    int[] top = {drawX, (drawY-1) };
                    int[] bottom= {drawX, (drawY+1) };
                    int[] left = {(drawX - 1), drawY };
                    int[] right = {(drawX + 1), drawY };
                    // Push the next ones into the stack
                    toDraw.push( (Object) top );
                    toDraw.push( (Object) bottom );
                    toDraw.push( (Object) left );
                    toDraw.push( (Object) right );
                }
            }
        }
    }
}