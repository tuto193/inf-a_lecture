import AlgoTools.IO;

public class Parenthesis {

    public static void main(String[] args) {
        IO.println();

        ArrayStack a = new ArrayStack();
        String b;
        boolean c = true;

        do {
            b = IO.readString( "Eingeben: " );
        } while( b.length() == 0 );

        for( int i = 0; i < b.length() && c; i++ ) {
            if( b.charAt(i) == '(' ) {
                a.push(b.charAt(i));
            } else if( b.charAt(i) == ')' ) {
                if( !a.isEmpty() && ( char ) a.top() == '(' ) {
                    a.pop();
                } else {
                    c = false;
                }
            }
        }

        if (a.isEmpty() && c) {
            IO.println("Jo!");
        } else {
            IO.println("Cool!");
        }

        IO.println();

    }
}
