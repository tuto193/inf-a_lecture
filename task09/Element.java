/**
 * A small class to represent an element entry on a list
 */
public class Element {
    // The data stored in the element
    private Object data;
    // The next element in the list
    private Element next;

    /** A constructor that allows  us to create an elemente,
     * add data to it and set it's next value straight away
     * */

    public Element( Object d, Element n ) {
        this.data = d;
        this.next = n;
    }

    // Some constructor that creates an element with data and no next
    public Element( Object d ) { this( d, null ); }

    // A Constructor for an empty element
    public Element() { this(null, null); }

    /**
     * Get the data store in the element
     * @return the Data pointed at by the element
     */
    public Object getData() { return this.data; }

    /**
     * Get the element after this one
     * @return the element after this one
     */
    public Element getNext() { return this.next; }

    /**
     * Set the value of the element that comes after this one
     */
    public void setNext( Element n ) { this.next = n; }
}