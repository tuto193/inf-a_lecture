import AlgoTools.IO;

/**
* Diese Klasse implementiert den ADT Stack mithilfe eines Arrays.
* Das Array dient als interner Speicher für die Objekte des Stacks. Falls ein
* neues Objekt hinzugefügt werden soll, wird ein neues Array, mit einer um 1
* vergrößerten Länge angelegt. In dieses Array wird an Index 0 das neue Objekt
* eingefügt und dahinter die Objekte des alten Arrays kopiert. Falls ein Objekt
* vom Stack entfernt werden soll, geschieht wird umgekehrt vorgegangen.
*/
public class ArrayStack {

    // Attribut
    private Object[] stack;

    // Konstruktor
    public ArrayStack(){
        this.stack = new Object[0];
    }

    /**
     * Diese Operation liefert true, falls der Stack leer ist, sonst false.
    */
    public boolean isEmpty(){
        if (this.stack.length == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Diese Operation legt das übergebene Element auf den Stack.
    */
    public void push(Object newObject){
        Object[] dummy = new Object[this.stack.length + 1];
        dummy[0] = newObject;
        for (int i = 1; i < dummy.length; i++) {
            dummy[i] = this.stack[i - 1];
        }
        this.stack = dummy;
    }

    /**
     * Diese Operation liefert das oberste Element des Stacks.
    */
    public Object top(){
        if (isEmpty()){
            return null;
        }
        return this.stack[0];
    }

    /**
     * Diese Operation entfernt das oberste Element des Stacks und gibt dieses
    * zurück.
    */
    public Object pop(){
        if (isEmpty()){
        return null;
        }
        Object returnObject = top();
        Object[] dummy = new Object[this.stack.length - 1];
        for (int i = 0; i < dummy.length; i++) {
        dummy[i] = this.stack[i + 1];
        }
        this.stack = dummy;
        return returnObject;
    }

    /**
     * Diese Operation gibt den Inhalt des Stacks auf der Konsole aus.
    * ACHTUNG: eigentlich geht das nicht mit einem Stack!!!
    */
    public void printStack(){
        for (int i = 0; i < this.stack.length; i++) {
            IO.println(this.stack[i]);
        }
    }

    public void printCoordinates() {
        for( int i = 0; i < this.stack.length; i++ ) {
            IO.println( ( (int[]) (this.stack[i]) )[0] + ", " + ((int[]) this.stack[i] )[1] );
        }
    }

}
