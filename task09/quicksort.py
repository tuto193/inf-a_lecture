
array : list = [7, 8, 2, 5, 6, 1, 3, 9]

def quicksort( A, lo, hi ):
    if lo < hi:
        p = partition( A, lo, hi )
        print()
        print("Quicksort " + str(lo) + " bis " + str((p-1)) )
        quicksort( A, lo, p - 1 )
        print("Quicksort " + str((p+1) ) + " bis " + str(hi) )
        quicksort( A, p + 1, hi )

def partition( A, lo, hi ) -> int:
    print("Paritioning")
    pivot = A[hi]
    print("Pivot Element P = " + str(pivot) )
    i = lo
    for j in range(lo, hi):
        if A[j] < pivot:
            if A[i] != A[j]:
                print("Swapping " + str(A[i]) + " with " + str(A[j]) )
                temp = A[i]
                A[i] = A[j]
                A[j] = temp
                print ( '[%s]' % ', '.join(map(str, A) ) )
            i = i + 1
    print("Swapping " + str(A[i]) + " with " + str(A[hi]) )
    temp2 = A[i]
    A[i] = A[hi]
    A[hi] = temp2
    print ( '[%s]' % ', '.join(map(str, A) ) )
    return i

print("Sorting our array: ")
print ( '[%s]' % ', '.join(map(str, array) ) )
quicksort(array, 0, len(array) - 1 )
print("Done sorting")
print ( '[%s]' % ', '.join(map(str, array) ) )