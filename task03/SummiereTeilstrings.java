import AlgoTools.IO;
public class SummiereTeilstrings {
    public static void main(String[]args) {
        //Variablen erstellen
        int ziffer= 0;
        int l = 1;
        int ergebnis= 0;
        //Zeichenkette einlesen
        String zahl = IO.readString("Bitte Zahl eingeben:");
        //Jede Stelle der Zeichenkette einlesen
        for (int k = zahl.length() - 1; k >= 0 ; k--){
            char check = zahl.charAt(k);
            if( check == ','){
                ziffer = 0;
                l = 1;
            } else {
                if( (int) check >= (int) '0' && (int)check <= (int) '9' ) {
                    ziffer = (int) check - 48;
                    ziffer = ziffer * l;
                    ergebnis= ergebnis + ziffer;
                    l= l * 10;
                }
            }
        }
        IO.println(ergebnis);


    }
}
