import AlgoTools.IO;
public class Ziffernwurzel {

    public static int stringSum( String zeichenkette ) {
        int ziffer = 0;
        int ergebnis = 0;
        for (int k = 0; k < zeichenkette.length(); k++){
            ziffer = ((int)zeichenkette.charAt(k))-48;
            ergebnis = ergebnis + ziffer;
        }
        return ergebnis;
    }
    public static void main(String[] args) {

        int ergebnis = 0;
        boolean frag = false;
        String zeichenkette = "";
        do{
            frag = false;
            zeichenkette = IO.readString("Bitte Zeichenkette eingeben:");

            for (int k = 0; k < zeichenkette.length(); k++){
                char check = zeichenkette.charAt(k);
                if( (int) check < (int) '0' || (int) check > (int) '9' ) {
                    frag = true;
                    break;
                }
            }

        }while( frag );

        ergebnis = stringSum( zeichenkette );
        while( ergebnis > 9 ) {
            ergebnis = stringSum( "" + ergebnis );
        }
        IO.println(ergebnis);
    }
}
