import AlgoTools.IO;

public class VokalPalindrom{
    public static void main(String[] args) {
//String einlesen
        String eingabe = IO.readString("Bitte ein Palindrom eingeben:");
        eingabe = eingabe.toLowerCase();
        //boolean einlesen
        boolean palindrom = true;
        //Variablen erstellen
        int k=0;
        int l= eingabe.length()-1;
        char ausK, ausL;
        //Abbruchbedingung
        while(k <= l && palindrom){
            //Untersuchen des Wortes(Palindrom?)
            ausK = eingabe.charAt(k);
            ausL = eingabe.charAt(l);

            if( ausK != 'a' && ausK != 'e' && ausK != 'i' && ausK != 'o' && ausK != 'u' ) {
                ausK = 0;
            }
            if( ausL != 'a' && ausL != 'e' && ausL != 'i' && ausL != 'o' && ausL != 'u' ) {
                ausL = 0;
            }

            if(ausK == ausL){
                palindrom = true;
                k= k+1;
                l=l-1;
            } else {
                palindrom = false;
            }
        }
        //Ausgabe für Palindrom (palindrom = true)
        if(palindrom){
            IO.println(eingabe + " " + "ist ein VokalPalindrom.");
        }else{
            //Ausgabe für kein Palindrom (palindrom = false)
            IO.println(eingabe + " " + "ist kein VokalPalindrom.");
        }
    }
}
