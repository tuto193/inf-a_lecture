package task03;

import AlgoTools.IO;

public class Kompression {
    public static void main(String[] args) {
//String einlesen
        String eingabe = IO.readString("Bitte ein Wort eingeben:");
        eingabe = eingabe.toLowerCase();
        //Variablen erstellen
        int k=0;
        int n = 0;
        char newK, oldK = 0;
        //Abbruchbedingung
        String ergebnis = "";
        while(k < eingabe.length() ) {
            //Untersuchen des Wortes(Palindrom?)
            newK = eingabe.charAt(k);
            if( oldK == 0 ) {
                oldK = newK;
            }
            if( newK == oldK ) {
                n++;
            } else {
                ergebnis = ergebnis + n + oldK;
                n = 1;
            }
            oldK = newK;

            k++;
        }
        //Ausgabe für Palindrom (palindrom = true)
        ergebnis = ergebnis + n + oldK;
        IO.println( "Ergebnis: " + ergebnis);
    }
}
