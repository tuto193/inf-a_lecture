import AlgoTools.IO;

/**
* Diese Klasse soll ein wunderschönes Weihnachtsbild zeichnen.
*/
public class Weihnachtsbild {
    public static void main(String[] args) {
        /// Erzeuge das Brett und den Baum
        Zeichenbrett myBrett = new Zeichenbrett();
        Weihnachtsbaum myCTree = new Weihnachtsbaum(myBrett);
        /// Zeichne den Baum auf das Brett
        myCTree.zeichne();
        // Zeichne ein paar Geschenke
        for( int i = 0; i < 9; i++ ) {
            Geschenk toDraw = new Geschenk(i, myBrett );
            toDraw.zeichne();
        }
        // Create some stars
        Stern star1 = new Stern( 6, 12, myBrett );
        Stern star2 = new Stern( 10, 40, myBrett );
        Stern star3 = new Stern( 15, 20, myBrett );
        /// Gebe das Brett aus
        for( int i = 0; i < 100; i++ ) {
            star1.zeichne();
            star2.zeichne();
            star3.zeichne();
            myBrett.gebeAus();
            Zeichenbrett.warte(100);
        }
    }
}
