import AlgoTools.IO;

public class Stern {
    private Zeichenbrett brett;
    private int originX;
    private int originY;
    private char shape;
    private static int size = 4;
    private boolean blink = false;

    /**
     * Our private constructor that sets default (invalid) values to the coordinates
     * @param brett     the board where to draw the star
     */
    private Stern( Zeichenbrett brett ) {
        this.brett = brett;
        this.originX = -1;
        this.originY = -1;
        this.shape = '*';
    }

    /**
     *      A more complete constructor where we give specific coordinates to our star.
     *      If they are not valid, the values of X and Y will be set invalid again
     * @param y     the Y coordinate
     * @param x     the X coordinate
     * @param brett the board where to draw the star
     */
    public Stern( int y, int x, Zeichenbrett brett ) {
        this( brett );
        if( validX(x) && validY(y) ) {
            this.originX = x;
            this.originY = y;
        } else {
            IO.println( "The given coordinates are not valid" );
        }
        int top, bottom, left, right;
        top     = originY - size;
        bottom  = originY + size;
        right   = originX + size;
        left    = originX - size;

        // Check whether the area where we want to draw is actually free
        for( int i = bottom; i > top; i-- ) {
            for( int j = left; j < right; j++ ) {
                // If it's not free, reset the values of originX and originY to invalid ones
                if( this.brett.getZeichenbrett()[i][j] != ' ' ) {
                    this.originX = -1;
                    this.originY = -1;
                    IO.println( "The area selected for the star is not empty " );
                }
            }
        }
    }

    /**
     * Check whether the given X coordinate is on our board and our star fits in
     * @param x     the coordinate to check
     * @return      true if the cooridnate is valid
     */
    private boolean validX( int x ) {
        return ( x > 4 && x < this.brett.getSpalten() - 70 );
    }

    /**
     *  Check whether the Y coordinate exists in the board and our star fits in
     * @param y the y coordinate to check
     * @return
     */
    private boolean validY( int y ) {
        return ( y > 4 && y < this.brett.getZeilen() - 15 );
    }

    /**
     * Draw our star onto its board
     */
    public void zeichne() {
        if( blink ) {
            this.shape = ' ';
        }
        /// It is a lot faster to hard-code it than to implement a nicer solution
        // Top two dots and Bottom two dots
        this.brett.setZeichenbrett(originY - 2, originX - 2, this.shape );
        this.brett.setZeichenbrett(originY - 2, originX + 2, this.shape );
        this.brett.setZeichenbrett(originY + 2, originX + 2, this.shape );
        this.brett.setZeichenbrett(originY + 2, originX - 2, this.shape );
        // Mid-Top dots and Mid-Bottom dots
        this.brett.setZeichenbrett(originY - 1, originX - 1, this.shape );
        this.brett.setZeichenbrett(originY - 1, originX + 1, this.shape );
        this.brett.setZeichenbrett(originY + 1, originX + 1, this.shape );
        this.brett.setZeichenbrett(originY + 1, originX - 1, this.shape );
        // Middle 5 dots
        // The only place where a loop is worthwhile
        for( int i = originX - 4; i < originX + 5; i++ ) {
            if( i % 2 == 0 ) {
                this.brett.setZeichenbrett( originY, i, this.shape );
            }
        }
        // Reset the value of our shape
        this.shape = '*';
        // blink
        blink = !blink;
    }
}