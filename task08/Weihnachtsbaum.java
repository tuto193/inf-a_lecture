import AlgoTools.IO;

/**
* Diese Klasse repraesentiert eine zweidimensionale Zeichnung eines
* Weihnachtsbaums aus ASCII-Zeichen. Der Baum wird rekursiv gezeichnet.
*/
public class Weihnachtsbaum {

    // Variablendeklaration
    private Zeichenbrett brett;
    private char ast, stamm, kerze, stern;
    private int hoehe, mitte;

    /**
     * Oeffentlicher Konstruktor der Klasse Weihnachtsbaum.
    */
    public Weihnachtsbaum(Zeichenbrett brett) {
        // Initialisierung aller Variablen auf Standard-Werte.
        this.brett = brett;
        this.ast = '+';
        this.stamm = 'X';
        this.kerze = '|';
        this.stern = '*';
        this.hoehe = 0;
        this.mitte = 0;
    }

    /**
     * Diese Operation stellt den Weihnachtsbaum auf dem Zeichenbrett dar.
    */
    public void zeichne() {

        // Die initiale Hoehe des Baums ergibt sich aus der Hoehe des Zeichenbretts
        // ohne den Rahmen.
        int hoehe = brett.getZeilen() - 2;

        // Der Baum soll eine von der Hoehe abhaengige Breite haben.
        int breite = hoehe * 2;

        // Korrektur, sodass der Baum optimal auf das Zeichenbrett der Groesse
        // 140x40 passt.
        breite -= breite/5;

        // Die Breite sollte ungerade sein, damit die Spitze des Baums immer aus
        // einem Zeichen besteht.
        if (breite % 2 == 0) {
            breite++;
        }

        // Die Mitte des Baums ist fuer die Orientierung im Zeichenbrett wichtig.
        mitte = brett.getSpalten() - breite / 2 - 3;

        // Zeichne den Stamm des Baums.
        // Der Stamm ist 6 Zeilen hoch.
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < brett.getSpalten(); j++) {
                // Der Stamm ist drei Zeichen breit.
                if (j > mitte - 2 && j < mitte + 2) {
                    brett.setZeichenbrett(hoehe, j, this.stamm);
                }
            }
            // Fuer jede Zeile des Stamms, wird die Gesamthoehe des Baums reduziert.
            hoehe--;
        }

        // Zeichne den restlichen Baum oberhalb des Stamms.
        zeichneRek(hoehe, breite, mitte);

    }

    /**
     * Diese Operation zeichnet den Weihnachtsbaum, ohne den Stamm, rekursiv auf
    * das Zeichenbrett.
    */
    private void zeichneRek(int hoehe, int breite, int mitte){

        // Rekursionsanfang.
        if (hoehe == 1) {
            return;
        }

        // Zeichnen der einzelnen Aeste.
        for (int i = 1; i < brett.getSpalten(); i++) {
            // Anfang und Ende der zu zeichnenden Zeile
            int anfangZeile, endeZeile;
            anfangZeile = mitte - breite / 2;
            endeZeile = mitte + breite / 2;

            if (i > anfangZeile && i < endeZeile ) {
                /// Set the Candles on top of the ends, if we are at one
                /// .... but just every two ends
                if( hoehe % 2 == 0 && (i == anfangZeile + 1 || i == endeZeile - 1 ) ) {
                    brett.setZeichenbrett(hoehe - 1, i, this.kerze );
                }

                brett.setZeichenbrett(hoehe, i, this.ast);
                /// If we are on top of the Christmas Tree we put the star on top
                if( hoehe == 3 ) {
                    brett.setZeichenbrett(hoehe - 1, i, this.stern );
                }
            }
        }

        // Rekursionsschritt.
        zeichneRek(hoehe - 1, breite - 2, mitte);
    }
}
