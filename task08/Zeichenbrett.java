import AlgoTools.IO;

/**
* Diese Klasse stellt ein Zeichenbrett zur Verfuegung. Auf diesem kann mit
* ASCII-Zeichen gezeichnet werden.
*/
public class Zeichenbrett {

    private char[][] zeichenBrett;

    /**
     * Oeffentlicher Konstruktor der Klasse Zeichenbrett.
    * Erstellt ein char-Array, welches das Zeichenbrett repraesentiert und fuellt
    * dieses mit Leerzeichen.
    */
    public Zeichenbrett() {

        // Das Zeichenbrett hat die Groesse des Konsolenfensters. (40*140)
        this.zeichenBrett = new char[40][140];

        // Das Zeichenbrett besteht aus Leerzeichen und hat einen Rahmen aus '#'.
        this.initialisiereZeichenbrett();
    }

    /**
     * Diese Operation
    */
    public void setZeichenbrett(int zeile, int spalte, char zeichen){
        zeichenBrett[zeile][spalte] = zeichen;
    }

    /**
     * Diese Operation liefert das Zeichenbrett zurück.
    */
    public char[][] getZeichenbrett(){
        return this.zeichenBrett;
    }

    /**
     * Diese Operation liefert die Zeilenanzahl des Zeichenbretts zurück.
    */
    public int getZeilen(){
        return this.zeichenBrett.length;
    }

    /**
     * Diese Operation liefert die Spaltenanzahl des Zeichenbretts zurück.
    */
    public int getSpalten(){
        return this.zeichenBrett[0].length;
    }

    /**
     * Diese Operation stellt das Zeichenbrett dar.
    */
    public void gebeAus() {
        // Stelle das Zeichenbrett Zeichen für Zeichen dar.
        for( int zeile = 0; zeile < zeichenBrett.length; zeile = zeile + 1 ) {
            for( int spalte = 0; spalte < zeichenBrett[zeile].length; spalte = spalte + 1 ) {
                IO.print(zeichenBrett[zeile][spalte]);
            }
            IO.println();
        }
    }

    /**
     * Diese Operation bereitet das Zeichenbrett vor. Es wird mit Leerzeichen
    * gefüllt.
    */
    private void initialisiereZeichenbrett() {
        int zeile, spalte;

        // Fülle Welt mit Leerzeichen
        for ( zeile = 0; zeile < zeichenBrett.length; zeile = zeile + 1 ) {
            for ( spalte = 0; spalte < zeichenBrett[zeile].length; spalte = spalte + 1 ) {
                zeichenBrett[zeile][spalte] = ' ';
            }
        }

        // Zeichne den Rahmen
        // Zeichne erste und letzte Spalte
        for ( zeile = 0; zeile < zeichenBrett.length; zeile = zeile + 1 ) {
            zeichenBrett[zeile][0] = '#';
            zeichenBrett[zeile][zeichenBrett[zeile].length - 1] = '#';
        }

        // Zeichne erste und letzte Zeile
        for ( spalte = 0; spalte < zeichenBrett[0].length; spalte = spalte + 1 ) {
            zeichenBrett[0][spalte] = '#';
            zeichenBrett[zeichenBrett.length - 1][spalte] = '#';
        }
    }

    /**
     * Diese Operation lässt das Programm die angegebenen Zeit in Millisekunden
    * warten.
    * Das ist in Java etwas umständlich und wurde deshalb bereits implementiert.
    */
    public static void warte(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) { };
    }

}
