import AlgoTools.IO;

public class Geschenk {
    private int originX;
    private int originY;
    private int height;
    private int width;
    private char shape;
    private Zeichenbrett Brett;
    private static boolean right = false;

    // Constructor Chaining
    /**
     * A constructor that assigns the size to our present automatically based on the number entered.
     * The constructor also gives a proper X coordinate to our present, near the closest tree or present.
     * If the present doesn't fit on the floor, it will show the user that no more presents can be drawn.
     * The origin is then set to -1
     * @param size      a number, which's modulo 3 is the size of our present.
     * @param brett     the board where to draw our present
     */
    public Geschenk( int size, Zeichenbrett brett) {
        this( brett );
        // We only accept 3 sizes, make sure it has proper values
        int rSize = size % 3;
        switch( rSize ) {
            case 0:
                this.width = 5;
                this.height = 2;
                break;
            case 1:
                this.width = 6;
                this.height = 3;
                break;
            case 2:
                this.width = 8;
                this.height = 4;
                break;
            default:
                IO.println( "Error while computing the size of the present" );
                break;
        }
        this.setX();
        // If one of the sides is already full, try the other
        if( this.originX == -1 ) {
            right = !right;
            this.setX();
        }
        right = !right;
        // If we still couldn't place it, print and Error message
        IO.println( "No more presents could be placed on the floor. There's no space" );
    }

    /**
     *  A simple constructor, that assigns default values to our Geschenk. If this Constructor is used wrongly,
     * the values that are set won't be of any use
     * @param brett     the board where to draw or Geschenk
     */
    private Geschenk( Zeichenbrett brett ) {
        this.Brett = brett;
        // Das Geschenk muss auf dem Boden sein
        this.originY = brett.getZeilen() - 2;
        this.shape = '@';
        this.originX = -1;
        this.height = -1;
        this.width = -1;
    }

    /**
     * Set the X coordinate of the present, based on an alternating side
     */
    private void setX() {
        int i = -1;
        if( !right ) {
            i = 1;
        } else {
            i = 100;
        }

        // Iterate through the floor near the tree, and find the first available spot
        char[][] checkBoard = Brett.getZeichenbrett();
        boolean foundX = false;
        boolean empty;
        for( ; i < Brett.getSpalten() - width - 2 && !foundX; i++ ) {
            empty = false;
            // If we are on an empty spot and near a present or the tree
            // check from the left side
            if( !right ) {
                // space for present
                char begin, end;
                begin = checkBoard[originY][i];
                end   = checkBoard[originY][i + this.width + 1 ];
                    if( begin == ' ' && (end == '@' || end == 'X') ) {
                    // And the spot is completely free and can be drawn on
                    for( int j = i; j < i + this.width + 1; j++ ) {
                        if( checkBoard[originY][j] == ' ' ) {
                            foundX = true;
                        } else {
                            foundX = false;
                            break;
                        }
                    }
                }
            } else {
                // space for present
                char begin, end;
                begin = checkBoard[originY][i - 1];
                end   = checkBoard[originY][i + this.width + 1 ];
                if( ( begin == '@' || begin == 'X') && end == ' ' ) {
                    // And the spot is completely free and can be drawn on
                    for( int j = i; j < i + this.width + 1; j++ ) {
                        if( checkBoard[originY][j] == ' ' ) {
                            foundX = true;
                        } else {
                            foundX = false;
                            break;
                        }
                    }
                    if( foundX ) i++;
                }
            }
            // Assign our origin if we were able to find one
            if( foundX ) {
                this.originX = i;
            }
        }
        // if we weren't able to find an X origin, it means that we cannot draw anymoro presents
        if( !foundX ) {
            this.originX = -1;
        }
    }

    /**
     * Draw this Geschenk onto its board
     */
    public void zeichne() {
        // Just presents that were able to be properly initialized should be drawn
        if( originX != -1 ) {
            int left, right, top, bottom;
            // The boundaries of our Geschenk
            left = originX;
            right = originX + width;
            top = originY - height;
            bottom = originY;

            // Draw the whole thing
            for( int i = bottom; i > top; i-- ) {
                for( int j = left; j < right; j++ ) {
                    this.Brett.setZeichenbrett(i, j, this.shape );
                }
            }
        }
    }
}