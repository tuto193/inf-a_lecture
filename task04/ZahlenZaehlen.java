import AlgoTools.IO;
public class ZahlenZaehlen {
    public static void main (String[] args){
        int maximum1 = 0;
        int[] eingabe;
        int[] rate;
        int n = 0;
        int i, j;

        //Array fordern
        do{
            eingabe = IO.readInts( "Int-Array eingeben: " );
        }while(eingabe.length < 1);

        // Maximum bestimmen
        for(i = 0 ; i < eingabe.length; i++){
            if(eingabe[i] > maximum1) {
            //eingabe 1 = Maximum & pos1 = i
            maximum1 = eingabe[i];
            }
        }
        // Die Laengue von rate ist abhaengig von der groessten eingegebenen Zahl
        rate = new int[ maximum1 + 1];

        // Wie oft kommt jede Zahl (Index) von rate in eingabe vor
        for( i = 0; i < rate.length; i++ ) {
            // jede Zahl kommt (zumindest) 0 Mal vor
            n = 0;
            for( j = 0; j < eingabe.length; j++ ) {
                if( eingabe[j] == i ) {
                    n++;
                }
            }
            rate[i] = n;
            // Zahl ausgeben, wenn sie zumindest 1mal vorkommt
            if( rate[i] > 0 ) {
                IO.println( i + ": " + rate[i] );
            }
        }
    }
}
