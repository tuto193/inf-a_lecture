import AlgoTools.IO;

public class Bigramme {

    public static void main(String[] args) {

        String eingabe;
        int[][] bigram = new int[26][26];
        int ord, ordzwei;
        int maximum;
        int stelleeins= 0 , stellezwei = 0;
        String abfrage = "Initialize";

        // Lese String für eingabe ein
        eingabe = IO.readString("Bitte einen String eingeben: ");
        // Wandle eingabe in Kleinbuchstaben um
        eingabe = eingabe.toLowerCase();
        // Durchlaufe jedes Zeichen von eingabe
        for( int pos = 0; pos < eingabe.length() - 1; pos++ ) {
        // Betrachte nur Zeichen von a - z auch für das nachfolgende Zeichen
            if( ( (eingabe.charAt(pos) >= 'a') && (eingabe.charAt(pos) <= 'z' ) ) // buchstabe1 ist kein Sonderzeichen
            && ( ( eingabe.charAt(pos + 1) >= 'a' ) && (eingabe.charAt(pos + 1) <= 'z' ) ) ) { // buchstabe2 ist auch keins
                /* Wandle sie in Indixes um */
                ord = ( (int) eingabe.charAt(pos) ) - ( (int) 'a' );
                ordzwei = ( (int) eingabe.charAt(pos + 1) ) - ( (int)'a' );
                // Zaehle jedes Bigramm in bigram hoch
                bigram[ord][ordzwei]++;
            }
        }
        // Suche Maximum von bigram
        maximum = 0;
        for( int j = 0 ; j < 26 ; j++ ) {
            for( int i = 0; i < 26; i++ ) {
                if( bigram[i][j] > maximum ) {
                    maximum = bigram[i][j];
                    stelleeins = i;
                    stellezwei = j;
                }
            }
        }
        // Gebe Maximum aus
        IO.println("Das haeufigste Bigramm ist _" + ((char)(stelleeins + ((int)'a'))) + ((char)((stellezwei) + ((int)'a'))) + "_ und kommt " + maximum + " mal vor.");
        IO.println();
        // Frage solange nach einer neuen Zahl, bis eine leere Eingabe getätigt wird
        while( abfrage.length() != 0 ) {
            // Frage neues Bigramm ab
            abfrage = IO.readString("Bitte Bigramm eingeben (oder nichts zum abbrechen): ");
            // Beachte nur die Eingabe von zwei Buchstaben
            if( ( abfrage.length() == 2 ) ) {
                // Gib die Anzahl der Bigramme aus
                IO.println("Das Bigramm _" + abfrage + "_ kommt " + bigram[((int)abfrage.charAt(0) - (int)'a')][((int)abfrage.charAt(1) - (int)'a')] + " mal vor.");
            }
            IO.println();
        }
    }
}
