import AlgoTools.IO;

public class BinarySearch {
    public static void main( String[] args ) {
        int floor, input, mid, top;
        int[] eingabe = { 0, 1, 4, 5, 16, 78, 99, 105, 200 };
        floor = 0;
        top = eingabe.length - 1;
        boolean found = false;

        input = IO.readInt( "Gib eine Zahl ein, nach der gesucht wird...");

        while( floor <= top || !found ) {
            mid = (floor + top) / 2;
            if( eingabe[ mid ] == input ) {
                IO.println( "Gefunden am index: " + mid );
                found = true;
            }
            if( eingabe[mid] > input ) {
                top = mid;
            } else {
                floor = mid + 1;
            }
        }
    }
}