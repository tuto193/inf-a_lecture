import AlgoTools.IO;

/**
 * Liest ein Labyrinth ein und berechnet mittels Backtracking/Tiefensuche
 * ohne Rekursion mit Hilfe eines Stacks einen Weg durch dieses.
 */
public class LabyrinthStack{

    char[][] lab;

    /** Freies Feld */
    private static final char FREI  = ' ';
    /** Mauer */
    private static final char MAUER = '#';
    /** Startfeld */
    private static final char START = 'S';
    /** Zielfeld */
    private static final char ZIEL  = 'Z';
    /** Besuchtes Feld das zum PFAD gehoert */
    private static final char PFAD  = '-';
    /** Besuchtes Feld das nicht zum PFAD gehoert */
    private static final char BESUCHT = 'x';

    /**
     * Erstellt via Konsoleneingabe ein Labyrinth
     */
    LabyrinthStack(){
        int breite = 0, hoehe = 0;

        // Lese Breite ein
        do {
            breite = IO.readInt("Breite des Labyrinths: ");
        } while(breite <= 0);

        // Lese Hoehe ein
        do {
            hoehe = IO.readInt("Hoehe des Labyrinths: ");
        } while(hoehe <= 0);

        // Lege Array an, welches ausgegeben wird
        char[][] out = new char[hoehe][];

        // Fuer jede Zeile
        for(int i = 0; i < hoehe; i++) {
            char[] zeile;

            // lies Zeile mit korrekter Anzahl von Buchstaben ein
            do {
                zeile = IO.readChars("Zeile " + i + " des Labyrinths: ");
            } while(zeile.length != breite);

            out[i] = zeile;
        }
        lab = out;

    }

    /**
     * Findet einen Weg durch das Labyrinth.
     * Findet den Startpunkt und ruft findeZielmitStack mit dem Startpunkt auf.
     * Gibt zurück, ob ein Weg gefunden wurde.
     */
    private boolean findeZiel() {
        // TODO
        int start_x = -1, start_y = -1;
        boolean found_S = false;
        int h = lab.length;
        int w = lab[0].length;
        for( int i = 0; i < h && !found_S; i++ ) {
            for( int j = 0; j < w && !found_S; j++) {
                if( lab[i][j] == START ) {
                    start_x = j;
                    start_y = i;
                    found_S = true;
                }
            }
        }
        if( !found_S ) {
            IO.println( "Error: No start could be found!" );
            return false;
        }
        return findeZielmitStack(start_x, start_y);
    }

    /**
     * Findet mittels eines Stack einen Weg durch das Labyrinth.
     * Gibt zurueck, ob ein Weg gefunden wurde.
     *
     * Basically a Depth-First-Search
     */
    private boolean findeZielmitStack(int startX, int startY) {
        // TODO
        LinkedList paths_stack = new LinkedList();
        int i, j, new_x = startX, new_y = startY;
        int h = lab.length;
        int w = lab[0].length;
        boolean found_end = false;
        int[] last_step = new int[2];
        int[] step = new int[2];
        step[0] = -1;
        step[1] = -1;
        do {
            last_step[0] = step[0];
            last_step[1] = step[1];
            // Add our possible next 4 directions for our path
            //      They are actually 3, since we are not going backwards, but ok
            /// Make sure that the coordinates stay within limits
            // Clip X to (within) the boundaries
            if( new_x <= 0 ) {
                new_x = 1;
            } else if ( new_x >= w ){
                new_x--;
            }
            // Clip Y to (within) the boundaries
            if( new_y <= 0 ) {
                new_y = 1;
            } else if( new_y >= h ) {
                new_y--;
            }
            int[] lef = {new_y, new_x - 1};
            int[] rig = {new_y, new_x + 1};
            int[] top = {new_y - 1, new_x};
            int[] bot = {new_y + 1, new_x};
            ////////// PATH FINDIN ///////////////////////////////////
            // Check if right is free
            if( lab[new_y][new_x + 1 ] == FREI || lab[new_y][new_x + 1 ] == ZIEL ) {
                paths_stack.prepend( (Object) rig);
            }
            // Check if left is free
            if( lab[new_y][new_x - 1] == FREI || lab[new_y][new_x - 1] == ZIEL ) {
                paths_stack.prepend( (Object) lef );
            }
            // Check if bottom is free
            if( lab[new_y + 1][new_x] == FREI || lab[new_y + 1][new_x] == ZIEL ) {
                paths_stack.prepend( (Object) bot );
            }
            // Check if top is free
            if( lab[new_y - 1][new_x] == FREI || lab[new_y - 1][new_x] == ZIEL ) {
                paths_stack.prepend( (Object) top );
            }

            ///////// STEP CHECKING ///////////////////////////////////////
            if( !paths_stack.isEmpty() ) {
                step = (int[]) paths_stack.getHead();
            }

            // We are stuck and cannot advance anywhere else
            if( last_step[0] == step[0] && last_step[1] == step[1] ) {
            // we need to mark this spot as visited and step back to another path
                lab[ step[0] ][ step[ 1 ] ] = BESUCHT;
                // Get the dead-end out of the way
                paths_stack.removeHead();
            } else {
                // There is/was a place to go, so we go there
                char step_tile = lab[ step[0] ][ step[1] ];
                if(  step_tile == ZIEL ) {
                    // either we are done
                    found_end = true;
                    break;
                } else {
                    // Or we carry on
                    lab[ step[0] ][ step[1] ] = PFAD;
                }
            }
            if( !paths_stack.isEmpty() ) {
                step = (int[]) paths_stack.getHead();
                new_x = step[1];
                new_y = step[0];
            } else {
                found_end = false;
                break;
            }

        } while( !paths_stack.isEmpty() );

        // Delete the visited ones, so that the labyrinth looks decent when printed out
        for( i = 0; i < h; i++ ) {
            for( j = 0; j < w; j++ ) {
                if( lab[i][j] == BESUCHT ) {
                    lab[i][j] = FREI;
                } else if( lab[i][j] == PFAD && !found_end ) {
                    // If we didn't actually found a way to the end, we should also delete the traces we left
                    lab[i][j] = FREI;
                }
            }
        }
        return found_end;
    }

    /**
     * Zeigt das Labyrinth auf dem Terminal an
     */
    private void druckeLabyrinth() {
        // TODO
        // ...fast beleidigend
        int h = lab.length;
        int w = lab[0].length;
        for( int i = 0; i < h; i++ ) {
            for( int j = 0; j < w; j++ ) {
                IO.print( lab[i][j] );
            }
            IO.println();
        }
    }


    /**
     * Main: Liest Labyrinth ein und findet einen Weg von S nach Z
     */
    public static void main(String[]args){

        // Lese Labyrinth ein
        LabyrinthStack labyrinth = new LabyrinthStack();

        // Gebe aus
        IO.println();
        IO.println("----- Original-Labyrinth: -----");
        labyrinth.druckeLabyrinth();
        IO.println();

        boolean gefunden = labyrinth.findeZiel();

        if(gefunden) {
            // Gebe aus
            IO.println("-----   Gefundener Weg:   -----");
            labyrinth.druckeLabyrinth();
        } else {
            IO.println("Kein Weg zum Ziel gefunden!");
        }

    }
}
