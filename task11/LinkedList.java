import AlgoTools.IO;

/**
* Diese Klasse implementiert eine Liste, die mit Verweisen arbeitet.
*/
public class LinkedList {

    // Attribute
    // Die Referenz auf das erste Element.
    private Element head;
    // Die Referenz auf das letzte Element.
    private Element tail;

    /**
     * Erzeugt eine neue leere Liste.
    */
    public LinkedList() {
        // Die null-Referenz zeigt auf kein Element.
        head = null;
        tail = null;
    }

    /**
     * Gibt `true` zurueck, genau dann wenn diese Liste keine Elemente enthaelt.
    */
    public boolean isEmpty() {
        return head == null;
    }

    /**
     * Daten des Kopfes abfragen.
    */
    public Object getHead() {
        // Pruefe, ob head existiert.
        if ( head == null ) {
            return null;
        }

        // Gebe die Daten von head zurueck.
        return head.getObject();
    }

    /**
     * Daten des Schwanzes abfragen.
    */
    public Object getTail() {
        // Pruefe, ob tail existiert.
        if ( tail == null ) {
            return null;
        }

        // Gebe die Daten von tail zurueck.
        return tail.getObject();
    }

    /**
     * Ein Element vorne anstellen.
    */
    public void prepend(Object obj) {
        // Erzeuge neues Element.
        Element elem = new Element(obj);

        // Stelle neues Element voran.
        elem.setNext(head);
        head = elem;

        // Falls die Liste leer war ...
        if ( tail == null ) {
            tail = elem;
        }
    }

    /**
     * Ein Element hinten anhaengen.
    */
    public void append(Object obj) {
        // Erzeuge neues Element.
        Element elem = new Element(obj);

        if (tail != null) {
            tail.setNext(elem);
        }

        tail = elem;

        // Falls die Liste leer war ...
        if ( head == null ) {
            head = elem;
        }
    }

    /**
     * Den Kopf entfernen.
    */
    public Object removeHead() {
        // Merke dir head.
        Element elem = head;
        if ( elem == null ) {
            return null;
        }

        // Passe head an.
        head = elem.getNext();
        elem.setNext(null);

        // Falls die Liste nur aus head besteht.
        if ( tail == elem ) {
            tail = null;
        }

        // Gebe entfernte Daten zurueck.
        return elem.getObject();
    }
}
