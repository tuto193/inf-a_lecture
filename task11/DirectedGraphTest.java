import AlgoTools.IO;

/**
* Dies Klasse dient zum Testen der Klasse DirectedGraph.
*/
public class DirectedGraphTest {

  public static void main(String[] args) {
    IO.println();

    int[][] graphMatrix = readInput();
    DirectedGraph graph = new DirectedGraph(graphMatrix);

    IO.print("Anzahl der Knoten im Graph (sollte 9 sein): ");
    IO.println(graph.numberOfVertices());
    IO.println();

    IO.print("Sind Knoten 1 und 5 adjazent? (sollte true sein) ");
    IO.println(graph.adjacent(1, 5));
    IO.print("Sind Knoten 3 und 8 adjazent? (sollte false sein) ");
    IO.println(graph.adjacent(3, 8));
    IO.println();

    IO.print("Nachbarknoten von 4 sind (sollte 1 3 6 sein): ");
    LinkedList neighborsOfVertex4 = graph.neighbors(4);
    while (!neighborsOfVertex4.isEmpty()) {
      IO.print(neighborsOfVertex4.removeHead() + " ");
    }
    IO.println();
    IO.print("Nachbarknoten von 3 sind (sollte 0 4 sein): ");
    LinkedList neighborsOfVertex3 = graph.neighbors(3);
    while (!neighborsOfVertex3.isEmpty()) {
      IO.print(neighborsOfVertex3.removeHead() + " ");
    }
    IO.println();
    IO.println();

    for (int i = 0; i < graph.numberOfVertices(); i++) {
      IO.print("Der Ausgangsgrad von Knoten " + i + " ist: ");
      IO.println(graph.outDegree(i));
    }
    IO.println("Die Ausgangsgrade sollten sein: 1 2 2 2 3 3 2 1 1");
    IO.println();

    for (int i = 0; i < graph.numberOfVertices(); i++) {
      IO.print("Der Eingangsgrad von Knoten " + i + " ist: ");
      IO.println(graph.inDegree(i));
    }
    IO.println("Die Eingangsgrade sollten sein: 2 2 1 2 2 3 1 3 1");
    IO.println();

  }

  /**
   * Liest einen Graphen von dem Terminal.
   */
  private static int[][] readInput() {
    int anzahlKnoten = 0;

    // Lese Breite ein
    do {
      anzahlKnoten = IO.readInt("Anzahl der Knoten des Graphen eingeben: ");
    } while(anzahlKnoten <= 0);
    IO.println();

    int breite = anzahlKnoten;
    int hoehe = anzahlKnoten;

    // Lege Array an, welches zureuckgegeben wird.
    int[][] graphMatrix = new int[hoehe][];

    IO.print("Bitte jede Zeile der GraphenMatrix eingeben mit der Laenge ");
    IO.println("" + breite);
    // Fuer jede Zeile.
    for(int i = 0; i < hoehe; i++) {
      int[] zeile;

      // Lese Zeile mit korrekter Anzahl von Zahlen ein.
      do {
        zeile = IO.readInts();
      } while(zeile.length != breite);

      graphMatrix[i] = zeile;
    }
    return graphMatrix;
  }
}
