import AlgoTools.IO;

/**
 * Small class to represent a graph taken from a read matrix
 * This graph assumes that:
 * Out Nodes = Y
 * In Nodes = X
 * so... graph[OUT][IN]
 */
public class DirectedGraph {
    private int num_vertices;
    private int[][] m_matrix;

    public DirectedGraph( int[][] graphMatrix ) {
        // If we don't receive a real NxN Matrix
        if( graphMatrix.length != graphMatrix[0].length ) {
            IO.println( "Error: Matrix is not quadratic" );
            return;
        }
        this.num_vertices = graphMatrix.length;
        this.m_matrix = new int[num_vertices][num_vertices];
        // Initialize our matrix with the values found inside our input graph
        for( int i = 0; i < num_vertices; i++ ) {
            for( int j = 0; j < num_vertices; j++ ) {
                if( graphMatrix[i][j] == 0 ) {
                    m_matrix[i][j] = 0;
                } else {
                    m_matrix[i][j] = 1;
                }
            }
        }
    }

    private boolean isValid( int index ) {
        if( index >= 0 && index < num_vertices ) {
            return true;
        }
        IO.println( "Error: index " + index + " is not valid");
        return false;
    }

    public int numberOfVertices() { return num_vertices; }


    public boolean adjacent( int u, int v ) {
        if( isValid( u ) && isValid( v ) ) {
            return m_matrix[u][v] == 1;
        }
        return false;
    }

    public LinkedList neighbors( int u ) {
        if( isValid( u ) ) {
            LinkedList adjacents = new LinkedList();
            // Check all adjacents from this node
            for( int i = 0; i < num_vertices; i++ ) {
                if( m_matrix[u][i] == 1 ) {
                    adjacents.append(i);
                }
            }
            // Return our list;
            return adjacents;
        }
        return null;
    }

    public int outDegree( int u ) {
        if( isValid( u ) ) {
            int count = 0;
            for( int i = 0; i < num_vertices; i++ ) {
                // If it has an out-node
                if( m_matrix[u][i] == 1 ) {
                    count++;
                    // If it is a loop to itself we count it twice
                    if( u == i ) {
                        count++;
                    }
                }
            }
            return count;
        }
        return 0;
    }

    public int inDegree( int u ) {
        if( isValid( u ) ) {
            int count = 0;
            for( int i = 0; i < num_vertices; i++ ) {
                // If it has an out-node
                if( m_matrix[i][u] == 1 ) {
                    count++;
                    // If it is a loop to itself we count it twice
                    if( u == i ) {
                        count++;
                    }
                }
            }
            return count;
        }
        return 0;
    }
}