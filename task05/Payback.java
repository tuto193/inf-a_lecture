import AlgoTools.IO;

public class Payback {

  public static void main(String[] args) {

    int[] paybackNummer = new int[16];
    int ergebnis = 0;
    int pruefziffer;
    // Lese die ISBN ein
    do {
      paybackNummer = IO.readInts("Bitte Payback-Nummer mit Pruefziffer (einzelne Ziffern durch Leerzeichen trennen): ");
    } while(paybackNummer.length != 16);
    IO.println();
    // Addiere die einzelnen Ziffern bis auf die letzte
    for(int i = 0; i < (paybackNummer.length - 1); i++) {
      // Addiere ungerade Stellen einfach
      if((i % 2) == 0) {
        ergebnis = ergebnis + 2 * paybackNummer[i];
        // Addiere gerade dreifach
      } else {
        ergebnis = ergebnis + paybackNummer[i];
      }
    }
    // Nimm die letzte Stelle von ergebnis
    pruefziffer = ergebnis % 10;
    // Subtrahiere pruefziffer von 10
    pruefziffer = 10 - pruefziffer;
    // Prüfe Prüfziffer mit letzter Ziffer von paybackNummer
    if(pruefziffer == paybackNummer[15]) {
      // Korrekte Pruefziffer
      IO.println("Die Pruefziffer der eingegebenen Payback-Nummer ist korrekt!");
      // Falsche Prüfziffer
    } else {
      paybackNummer[15] = pruefziffer;
      IO.println("Die Pruefziffer der eingegebenen Payback-Nummer ist nicht korrekt!");
      IO.print("Die Korrekte Payback-Nummer ist: ");
      for(int j = 0; j < paybackNummer.length; j++) {
        IO.print(paybackNummer[j], 2);
      }
      IO.println();
    }
  }
}
