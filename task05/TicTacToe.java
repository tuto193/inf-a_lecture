import AlgoTools.IO;

/**
* Diese Klasse modelliert das Spiel Tic-Tac-Toe.
* Bei dem Spiel Tic-Tac-Toe setzen auf einem quadratischen, 3 x 3 Felder grossen
* Spielfeld zwei Spieler abwechselnd ihr Zeichen (ein Spieler Kreuze, der andere
* Kreise) in ein freies Feld. Der Spieler, der als Erster drei Zeichen in eine
* Zeile, Spalte oder Diagonale setzen kann, gewinnt.
*/
public class TicTacToe {

    /**
     * Diese Methode initialisiert das Spielfeld mit Leerzeichen.
    *
    * @param spielfeld Spielfeld als zweidimensionales char-Array
    * @return Spielfeld als zweidimensionales char-Array
    */
    public static char[][] initialisiereSpielfeld(char[][] spielfeld) {
        int w = 3;
        int h = 3;
        for( int i = 0; i < h; i++ ) {
            for( int j = 0; j < w; j++ ) {
                spielfeld[i][j] = ' ';
            }
        }
        return spielfeld;
    }

    /**
     * Diese Methode gibt das Spielfeld auf der Konsole aus.
    *
    * @param spielfeld Spielfeld als zweidimensionales char-Array
    */
    public static void gebeSpielfeldAus(char[][] spielfeld) {
        int pad = 3;
        for( int i = 0; i < 4; i++ ) {
            for( int j = 0; j < 4; j++ ) {
                // We are at the top
                if( i == 0 ) {
                    if( j == 0 ) {
                        IO.print( " ", pad );
                    } else { // Print Column instructions
                        IO.print( j - 1, pad );
                    }
                } else if( j == 0 ) {// Check if we need to print the row instructions
                    IO.print( i - 1, pad );
                } else { // We are otherwise inside the field
                    IO.print( spielfeld[i - 1][j - 1], pad );
                }
            }
            IO.println();
        }
    }

    /**
     * Diese Methode laesst den aktiven Spieler seinen Zug ausfuehren.
    *
    * @param spielfeld Spielfeld als zweidimensionales char-Array
    * @param spielerZeichen Das Zeichen eines Spielers ist X, das des anderen O
    * @return Koordinaten der Eingabe des aktiven Spielers
    */
    public static int[] fuehreZugAus(char[][] spielfeld, char spielerZeichen) {
        int[] turn = new int[2];
        boolean validTurn = false;
        while( !validTurn ) {
            turn[0] = IO.readInt( "Spieler " + spielerZeichen + ", geben Sie eine Zeile ein [0-2]: " );
            turn[1] = IO.readInt( "Spieler " + spielerZeichen + ", geben Sie eine Spalte ein [0-2]: " );
            validTurn = checkValid( turn, spielfeld );
        }
        return turn;
    }

    /**
     * Kontrolliert, ob die eingegebenen Koordinaten im Feld
     * ein freies Feld sind (" ")
     * @param turn      die Koordinaten des Zuges
     * @param field     Das Spielfeld zu Kontrollieren
     * @return          True, wenn " ", sonst False
     */
    public static boolean checkValid( int[] turn, char[][] field ) {
        if( turn[0] < 3 && turn[0] >= 0 && turn[1] >= 0 && turn[1] < 3 ) {
            if( field[ turn[0] ][ turn[1] ] == ' ' ) {
                return true;
            } else {
                IO.println( "Das eingegebene Feld ist nicht gueltig. ");
                return false;
            }
        }
        IO.println( "Das eingegebene Feld existiert nicht");
        return false;
    }
    /**
     * Hauptmethode der Klasse TicTacToe.
    * HIER SOLL NICHTS GEAENDERT WERDEN!
    *
    * @param args Kommandozeilenparameter
    */
    public static void main(String[] args) {

        // VARIABLENDEKLARATION.
        // Das Spielfeld besteht aus 3 x 3 Feldern.
        /** Das Spielfeld als zweidimensionales char-Array. */
        char[][] spielfeld = new char[3][3];

        // Spieler X faengt an, da zu Beginn des Spielablaufs direkt ein Spieler-
        // wechsel erfolgt muss hier das Zeichen 'O' eingetragen werden.
        /** Das Zeichen des Spielers. 'O' oder 'X' */
        char spielerZeichen = 'O';

        // Diese Variable gibt an, ob ein weiterer Zug gemacht werden kann (0), ein
        // Spieler gewonnen hat (1) oder das Spiel unentschieden ausgegangen
        // ist (2).
        /** Gibt an, ob das Spiel beendet wurde. */
        int spielBeendet = 0;

        // In diesem Array werden die vom Spieler eingegebene Zeile (Index 0) und
        // Spalte (Index 1) gespeichert.
        /** Koordinaten, an denen der aktive Spieler sein Zeichen setzt. */
        int[] eingabeKoordinaten = new int[2];

        // =========================================================================
        // ====== Spielvorbereitung
        // =========================================================================
        gebeSpielregelnAus();

        // Das Spielfeld wird mit Leerzeichen gefuellt.
        spielfeld = initialisiereSpielfeld(spielfeld);

        // Diese Schleife wird solange wiederholt, bis das Spiel entweder von einem
        // der beiden Spieler gewonnen oder unentschieden beendet wurde.
        do {
            // =======================================================================
            // ====== SPIELERWECHSEL
            // =======================================================================
            // Zu Beginn jedes Zuges erfolgt ein Spielerwechsel.
            if (spielerZeichen == 'X') {
                spielerZeichen = 'O';
            } else {
                spielerZeichen = 'X';
            }

            // =======================================================================
            // ====== AUSGABE
            // =======================================================================
            // Das Spielfeld wird auf der Konsole ausgegeben.
            gebeSpielfeldAus(spielfeld);

            // =======================================================================
            // ====== Zug ausfuehren
            // =======================================================================
            eingabeKoordinaten = fuehreZugAus(spielfeld, spielerZeichen);
            // Setze das Zeichen des Spielers an die Eingabekoordinaten.
            spielfeld[eingabeKoordinaten[0]][eingabeKoordinaten[1]] = spielerZeichen;

            // =====================================================================
            // ====== SPIELENDE PRUEFEN
            // =====================================================================
            spielBeendet = pruefeObSpielBeendet(spielfeld, eingabeKoordinaten,
            spielerZeichen);

            // Wiederhole, solange kein Spieler gewonnen hat oder das Spiel nicht
            // unentschieden ausgegangen ist.
        } while (spielBeendet == 0);

        // Es muss noch einmal das Spielfeld ausgegeben werden, damit die Dreier-
        // reihe sichtbar wird.
        gebeSpielfeldAus(spielfeld);

        // =========================================================================
        // ====== Spiel beendet, Ausgabe Sieger
        // =========================================================================
        // An dieser Stelle steht in der Variablen spielBeendet eine 1 oder eine 2.
        if (spielBeendet == 1) {
            IO.println("SPIELER " + spielerZeichen + " HAT GEWONNEN!");
        } else {
            IO.println("UNENTSCHIEDEN!");
        }
        IO.println();

    // Ende der main()
    }

    /**
     * Diese Methode gibt die Spielregeln auf der Konsole aus und zögert den
    * Beginn des Spiels solange heraus, bis die return-Taste gedrueckt wird.
    * HIER SOLL NICHTS GEAENDERT WERDEN!
    */
    public static void gebeSpielregelnAus() {
        do {
            IO.println("TIC-TAC-TOE");
            IO.println();
            IO.println("Spielregeln: ");
            IO.println("Auf einem quadratischen, 3×3 Felder grossen Spielfeld ");
            IO.println("setzen zwei Spieler abwechselnd ihr Zeichen (ein ");
            IO.println("Spieler Kreuze, der andere Kreise) in ein freies Feld. ");
            IO.println("Der Spieler, der als Erster drei Zeichen in eine Zeile, ");
            IO.println("Spalte oder Diagonale setzen kann, gewinnt.");
            IO.println();
            IO.println("Weiter mit return!");
        } while ( (int) IO.readChar() == 13 );
    }

    /**
     * Diese Methode prueft ob einer der Spieler gewonnen hat oder ob das Spiel
    * unentschieden ausgegangen ist.
    * HIER SOLL NICHTS GEAENDERT WERDEN!
    *
    * @param spielfeld Spielfeld als zweidimensionales char-Array
    * @param eingabeKoordinaten Koordinaten der Eingabe des aktiven Spielers
    * @param spielerZeichen Das Zeichen eines Spielers ist X, das des anderen O
    * @return int: 0 = Spiel geht weiter, 1 = Spiel wurde gewonnen,
    *   2 = Spiel ist unentschieden
    */
    public static int pruefeObSpielBeendet(char[][] spielfeld,
    int[] eingabeKoordinaten, char spielerZeichen) {
        // Wir gehen davon aus, dass das Spiel unentschieden ausgegangen ist.
        boolean spielUnentschieden = true;
        int zeichenZaehler = 0;

        // Pruefe beide Diagonalen ob Spiel gewonnen wurde.
        if (spielfeld[0][0] != ' ' && spielfeld[0][0] == spielfeld[1][1] &&
        spielfeld[0][0] == spielfeld[2][2] || spielfeld[0][2] != ' ' &&
        spielfeld[0][2] == spielfeld[1][1] && spielfeld[0][2] == spielfeld[2][0]) {
            return 1;
        }
        else {
            // Pruefe Zeile in der das aktuelle Zeichen eingetragen wurde.
            for (int spalte = 0; spalte < spielfeld[eingabeKoordinaten[0]].length;
            spalte = spalte + 1) {
                if (spielerZeichen == spielfeld[eingabeKoordinaten[0]][spalte]) {
                    zeichenZaehler++;
                }
            }
            // Falls die Zeichen in der Zeile alle gleich waren ist das Spiel zuende.
            if (zeichenZaehler == 3) {
                return 1;
            }
            else {
                zeichenZaehler = 0;
                // Pruefe Spalte in der das aktuelle Zeichen eingetragen wurde.
                for (int zeile = 0; zeile < spielfeld.length; zeile = zeile + 1) {
                    if (spielerZeichen == spielfeld[zeile][eingabeKoordinaten[1]]) {
                        zeichenZaehler++;
                    }
                }
                // Falls die Zeichen in der Spalte alle gleich waren ist das Spiel zuende.
                if (zeichenZaehler == 3) {
                    return 1;
                }
            }
        }

        // Pruefe auf unentschieden.
        for (int zeile = 0; zeile < spielfeld.length; zeile = zeile + 1) {
            for (int spalte = 0; spalte < spielfeld[zeile].length;
            spalte = spalte + 1) {
                if (spielfeld[zeile][spalte] == ' ') {
                    spielUnentschieden = false;
                }
            }
        }
        if (spielUnentschieden) {
            return 2;
        }

        // Das Spiel geht weiter, da nicht unentschieden oder einer der Spieler
        // gewonnen hat.
        return 0;
    }

    // Ende der Klasse
}
